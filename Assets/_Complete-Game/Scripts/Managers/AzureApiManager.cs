﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft;
using Newtonsoft.Json;
using CompleteProject;

namespace CompleteProject
{
    public class AzureApiManager : MonoBehaviour
    {

        private static string URI = "https://westus.api.cognitive.microsoft.com/face/v1.0/detect?returnFaceAttributes=emotion";
        private static string API_KEY = "YOUR_API_KEY";

        public WebCamManager webCamManager;

        public EnemyManager enemyManager;
        public Text emotionsText;

        private Emotion lastEmotion;

        private bool newEmotionSincePreviousGet = false;

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            if (webCamManager.IsNewImageSincePreviousGet())
            {
                StartCoroutine(GetVisionDataFromByteArray(webCamManager.GetLastImage()));
            }
        }

        public bool IsNewEmotionSincePreviousGet()
        {
            return newEmotionSincePreviousGet;
        }

        public Emotion GetLastEmotion()
        {
            newEmotionSincePreviousGet = false;
            return lastEmotion;
        }

        IEnumerator GetVisionDataFromByteArray(byte[] array)
        {
            var headers = new Dictionary<string, string>() {
                { "Ocp-Apim-Subscription-Key", API_KEY },
                { "Content-Type", "application/octet-stream" }
            };

            WWW www = new WWW(URI, array, headers);

            yield return www;
            string responseData = www.text; // Save the response as JSON string
            Debug.Log("<color=brown>Reading emotions: </color>");
            Debug.Log("<color=brown>" + responseData + "</color>");
            Response[] response = JsonConvert.DeserializeObject<Response[]>(responseData);
            if (response.Length > 0 && response[0].faceAttributes != null)
            {
                newEmotionSincePreviousGet = true;
                lastEmotion = response[0].faceAttributes.emotion;
                Debug.Log("<color=red>" + JsonConvert.SerializeObject(lastEmotion) + "</color>");
            }
        }
    }
}