﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft;
using Newtonsoft.Json;
using CompleteProject;

namespace CompleteProject
{
    public class WebCamManager : MonoBehaviour
    {
        static WebCamTexture webCamTexture;
        public RawImage rawimage;

        private static readonly float TIME_INTERVAL = 10.0f;
        private float time = 2.0f;

        private bool newImageSincePreviousGet = false;
        private byte[] lastImage;

        void Start()
        {

            if (webCamTexture == null)
                webCamTexture = new WebCamTexture();
            rawimage.texture = webCamTexture;
            rawimage.material.mainTexture = webCamTexture;
            if (!webCamTexture.isPlaying)
                webCamTexture.Play();

        }

        void Update()
        {
            if (time >= 0)
            {
                time -= Time.deltaTime;
                return;
            }
            else
            {
                time = TIME_INTERVAL;
                StartCoroutine(TakePhoto());
            }
        }

        public bool IsNewImageSincePreviousGet()
        {
            return newImageSincePreviousGet;
        }

        public byte[] GetLastImage()
        {
            newImageSincePreviousGet = false;
            return lastImage;
        }

        private IEnumerator TakePhoto()
        {

            Debug.Log("<color=orange>Photo is taking...</color>");
            Texture2D photo = new Texture2D(webCamTexture.width, webCamTexture.height);
            photo.SetPixels(webCamTexture.GetPixels());
            photo.Apply();

            byte[] bytes = photo.EncodeToPNG();
            lastImage = bytes;
            newImageSincePreviousGet = true;
            yield return new WaitForEndOfFrame();
        }
    }
}