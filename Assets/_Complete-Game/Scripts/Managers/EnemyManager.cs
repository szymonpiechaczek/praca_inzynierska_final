﻿using UnityEngine;

namespace CompleteProject
{
    public class EnemyManager : MonoBehaviour
    {
        public PlayerHealth playerHealth;       // Reference to the player's heatlh.
        public GameObject enemy;                // The enemy prefab to be spawned.
        public float spawnTime = 3f;            // How long between each spawn.
        public Transform[] spawnPoints;         // An array of the spawn points this enemy can spawn from.
        public GameObject player;


        void Start ()
        {
            // Call the Spawn function after a delay of the spawnTime and then continue to call after the same amount of time.
            InvokeRepeating ("Spawn", spawnTime, spawnTime);
            player = GameObject.FindGameObjectWithTag("Player");

        }


        void Spawn ()
        {
            // If the player has no health left...
            if(playerHealth.currentHealth <= 0f)
            {
                // ... exit the function.
                return;
            }

            // Find a random index between zero and one less than the number of spawn points.
            int spawnPointIndex = Random.Range (0, spawnPoints.Length);

            // Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
            Instantiate (enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
        }


        public void SpawnNextToPlayer()
        {
            if (playerHealth.currentHealth <= 0f)
            {
                // ... exit the function.
                return;
            }
            var randX = GetRandomDistance();
            var randZ = GetRandomDistance();
            Instantiate(enemy, new Vector3(player.transform.position.x + randX, 0, player.transform.position.z + randZ), player.transform.rotation);
        }

        int GetRandomDistance()
        {
            int min = 2;
            int max = 8;
            bool sign = Random.Range(0, 10) > 5 ? true : false;

            return sign ? Random.Range(min, max) : Random.Range(min*-1, max*-1);
        }

    }
}