﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft;
using Newtonsoft.Json;
using CompleteProject;

namespace CompleteProject
{
    public class EmotionManager : MonoBehaviour
    {
        public AzureApiManager azureApiManager;


        public EnemyManager enemyManager;
        public Text emotionsText;

        GameObject player;
        PlayerHealth playerHealth;

        GameObject gunBarrelEnd;
        PlayerShooting playerShooting;

        // Use this for initialization
        void Start()
        {
            player = GameObject.FindGameObjectWithTag("Player");
            playerHealth = player.GetComponent<PlayerHealth>();

            gunBarrelEnd = GameObject.FindGameObjectWithTag("GunBarrelEnd");
            playerShooting = gunBarrelEnd.GetComponent<PlayerShooting>();
        }

        // Update is called once per frame
        void Update()
        {
            if (azureApiManager.IsNewEmotionSincePreviousGet())
            {
                ManageEmotion(azureApiManager.GetLastEmotion());
            }
        }


        void ManageEmotion(Emotion emotion)
        {
            emotionsText.text = GetEmotionString(emotion);
            if (emotion.fear > 0.02 || emotion.sadness > 0.02)
            {
                HandleFearOrSadness();
            }
            else if (emotion.anger > 0.02 || emotion.contempt > 0.02 || emotion.disgust > 0.02)
            {
                HandleAnger();
            }
            else if (emotion.happiness > 0.1f)
            {
                HandleHappiness(emotion.happiness);
            }
            else
            {
                HandleNeutralEmotion();
            }
        }

        void HandleNeutralEmotion()
        {
            int enemies = Random.Range(2, 5);
            for (int i = 0; i < enemies; i++)
                enemyManager.SpawnNextToPlayer();
            emotionsText.text += "\nBe more emotional, man!";
        }

        void HandleFearOrSadness()
        {
            playerHealth.GetHealed(50);
            playerShooting.Explode(12f, 400);
            emotionsText.text += "\nCome on, bro! Smile";
        }

        void HandleAnger()
        {
            playerShooting.Explode(10f, 100);
            emotionsText.text += "\nThat's called true rage!";
        }

        void HandleHappiness(float happiness)
        {

            if (happiness > 0.7f)
            {
                int enemies = Random.Range(2, 10);
                for (int i = 0; i < enemies; i++)
                    enemyManager.SpawnNextToPlayer();
            }
            int healTreshold = (int)Random.Range(happiness * 10, happiness * 30);
            if (playerHealth.currentHealth > healTreshold)
            {
                playerHealth.TakeDamage(healTreshold);
            }

            emotionsText.text += "\nYou're too happy...";
        }

        string GetEmotionString(Emotion emotion)
        {
            var res = "Your emotions:"
                + "\nanger\t\t\t" + emotion.anger
                + "\ncontempt\t\t" + emotion.contempt.ToString("0.00")
                + "\ndisgust\t\t\t" + emotion.disgust.ToString("0.00")
                + "\nfear\t\t\t\t" + emotion.fear.ToString("0.00")
                + "\nhappiness\t" + emotion.happiness.ToString("0.00")
                + "\nneutral\t\t\t" + emotion.neutral.ToString("0.00")
                + "\nsadness\t\t" + emotion.sadness.ToString("0.00")
                + "\nsurprise\t\t" + emotion.surprise.ToString("0.00");
            return res;
        }
    }
}