﻿using System;

[Serializable]
public class FaceRectangle
{
    public int top;
    public int left;
    public int width;
    public int height;
}