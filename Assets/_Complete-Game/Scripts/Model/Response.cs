﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Response : MonoBehaviour
{
    public string faceId;
    public FaceRectangle faceRectangle;
    public FaceAttributes faceAttributes;
}